# Boas-vindas, este é o desafio técnico de QE para o Sicredi.
[![pipeline status](https://gitlab.com/daakemii/teste-qe-sicredi/badges/main/pipeline.svg)](https://gitlab.com/daakemii/teste-qe-sicredi/-/commits/main)
# Introduçãoo
    - Projeto responsável pelos testes do desafio técnico para a vaga QA Engineer do sicredi

### Tecnologias Utilizadas

* [Java](https://www.java.com/pt-BR) - Linguagem utilizada para a escrita dos testes
* [Maven](https://maven.apache.org) - Gerenciador de dependências
* [Rest-assured](https://rest-assured.io) - Framework responsável por fazer chamadas em API Rest.
* [IntelliJ](https://www.jetbrains.com/idea/) - IDE padrão do projeto

### Instalação / Execução dos Testes
Para executar os testes há duas formas.

Executando pelo Maven.

```ssh
$ Clonar o projeto
$ Acessar a raíz do projeto ( Onde esta localizado o arquivo pom.xml)
$ Executar o comando 'mvn test'

```

Executando pelo Runner do Junit
- Necessária a configuração do SDK em 'File -> Project Structure' utilizar a SDK Default do java 11.
- Necessário configurar o profile para execução local dos testes


Configurar profile no Intellij

```ssh
$ Acessar o menu Run
$ Clicar em Edit Configurations
$ Abrir opção 'Junit' -> 'Runner'
$ No campo 'Environment Variables' informar "profile=local"
Clicar em Apply e em OK
```

Executar pelo Intellij

```ssh
$ Clonar o projeto
$ Abrir o projeto no Intellij
$ Acessar o caminho 'src/test/java/runners'
$ Com a classe Runner.java aberta clicar com o botão direto do mouse em 'Run Runner'
```

# Bugs

Bug#0001 Ao realizar o cadastro de um produto sem informar nenhum dado, retorna 200 - ok e o id 101, deveria apresentar o status code 400 - bad request e uma mensagem de validação de quais campos estão vazios ou são obrigatorios.


# Sugestões

Validações do corpo das requisições.

Retorno correto do status code, ao realizar um cadastro ou auth deveria retornar 201 e não 200.

