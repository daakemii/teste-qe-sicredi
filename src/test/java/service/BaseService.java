package service;

import base.util.PropertiesUtil;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static base.RequestManager.shared;
import static io.restassured.RestAssured.given;

public class BaseService {
    protected static final String DUMMY_URI = "dummyJson.baseURI";
    protected PropertiesUtil propertiesUtil;

    public Response doGetRequest(String resource) {
        return given()
                .spec(shared().getRequest())
                .when()
                .get(resource);
    }

    public Response doGetRequestWithToken(String token, String resource) {
        return given()
                .spec(shared()
                .getRequest())
                .header("Authorization", "Bearer " + token)
                .when()
                .get(resource);
    }


    public Response doPostRequestWithPayload(String payloadJson, String resource) {
        return given()
                .spec(shared()
                .getRequest())
                .contentType(ContentType.JSON)
                .body(payloadJson)
                .when()
                .post(resource);
    }

}
