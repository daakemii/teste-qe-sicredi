package service;

import base.util.PropertiesUtil;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import java.io.IOException;

import static base.RequestManager.*;

public class ApplicationStatusService extends BaseService {

    private static final String TEST_ENDPOINT = "/test";

    public ApplicationStatusService() throws IOException {
        propertiesUtil = new PropertiesUtil();
        shared().setBaseURI(propertiesUtil.getPropertyByName(DUMMY_URI));
    }

    @Step("executes a request to check the status of the application")
    public Response getTestStatus() {
        return doGetRequest(TEST_ENDPOINT);
    }


}
