package service;

import base.util.PropertiesUtil;
import io.restassured.response.Response;
import org.json.JSONObject;

import java.io.IOException;

import static base.RequestManager.shared;
import static base.util.Constants.SLASH;

public class ProductService extends BaseService {
    private static final String PRODUCTS_ENDPOINT = "/products";
    private static final String ADD_PRODUCTS_ENDPOINT = "/add";

    public ProductService() throws IOException {
        propertiesUtil = new PropertiesUtil();
        shared().setBaseURI(propertiesUtil.getPropertyByName(DUMMY_URI));
    }

    public Response getProductList() {
        return doGetRequest(PRODUCTS_ENDPOINT);
    }

    public Response getProduct(int id) {
        return doGetRequest(PRODUCTS_ENDPOINT + SLASH + id);
    }

    public Response postProduct(JSONObject payload) {
        return doPostRequestWithPayload(payload.toString(), PRODUCTS_ENDPOINT + ADD_PRODUCTS_ENDPOINT);
    }

}
