package service;

import base.util.PropertiesUtil;
import io.restassured.response.Response;

import java.io.IOException;

import static base.RequestManager.shared;

public class UserService extends BaseService {

    private static final String USER_ENDPOINT = "/users";

    public UserService() throws IOException {
        propertiesUtil = new PropertiesUtil();
        shared().setBaseURI(propertiesUtil.getPropertyByName(DUMMY_URI));
    }

    public Response getUserList() {
        return doGetRequest(USER_ENDPOINT);
    }

}
