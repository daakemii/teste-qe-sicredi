package service;

import base.util.PropertiesUtil;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.json.JSONObject;

import java.io.IOException;

import static base.RequestManager.shared;
import static base.util.Constants.PASSWORD_FIELD;
import static base.util.Constants.USERNAME_FIELD;

public class AuthService extends BaseService {

    private static final String AUTH_ENDPOINT = "/auth";
    private static final String LOGIN_ENDPOINT = "/login";
    private static final String PRODUCTS_ENDPOINT = "/products";

    public AuthService() throws IOException {
        propertiesUtil = new PropertiesUtil();
        shared().setBaseURI(propertiesUtil.getPropertyByName(DUMMY_URI));
    }

    @Step("Performs user authentication")
    public Response doAuthentication(final String username, final String password) {
        JSONObject payload = new JSONObject();
        payload.put(USERNAME_FIELD, username);
        payload.put(PASSWORD_FIELD, password);

        return doPostRequestWithPayload(payload.toString(), AUTH_ENDPOINT + LOGIN_ENDPOINT);
    }

    @Step("Queries products for an authenticated user")
    public Response getUserAuthenticationProducts(String token) {
        return doGetRequestWithToken(token, AUTH_ENDPOINT + PRODUCTS_ENDPOINT);
    }

}
