package runners;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import suites.AllTests;
import test.applicationStatus.ApplicationStatusTest;
import test.auth.AuthTest;
import test.product.ProductTest;
import test.user.UserTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(AllTests.class)
@Suite.SuiteClasses({
        ApplicationStatusTest.class,
        AuthTest.class,
        UserTest.class,
        ProductTest.class
})
public class Runners {
}
