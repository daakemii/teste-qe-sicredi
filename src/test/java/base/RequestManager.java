package base;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.Getter;
import lombok.Setter;
import org.json.JSONObject;

import java.util.Map;

public class RequestManager {
    private static RequestManager sharedInstance;
    private RequestSpecification request;
    @Setter
    @Getter
    private Response response;
    @Getter
    private String profile;
    @Getter
    @Setter
    private JSONObject payloadJson;
    @Getter
    @Setter
    private Map<String, Object> payloadMap;

    private RequestManager (){

    }

    public static synchronized RequestManager shared() {
        if (sharedInstance == null) sharedInstance = new RequestManager();

        return sharedInstance;
    }

    public RequestSpecification getRequest() {
        if (request == null) request = new RequestSpecBuilder().build();

        return request;
    }

    public void setBaseURI(final String uri) {
        this.getRequest().baseUri(uri);
    }

    public void setProfile(final String profile) {
        if (profile == null || !profile.trim().equalsIgnoreCase("local")) this.profile = "default";
        else this.profile = profile.trim().toUpperCase();
    }

    public void teardownRequest() {
        this.request = null;
    }

    public void teardownRequestResponse() {
        this.response = null;
    }
}
