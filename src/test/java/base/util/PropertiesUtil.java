package base.util;

import base.ProjectSettings;
import base.RequestManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class PropertiesUtil {

    public static final String LOCAL_PROPERTY_FILE_NAME = "application-local.properties";
    public static final String DEFAULT_PROPERTY_FILE_NAME = "application.properties";

    BufferedReader reader;
    Properties properties = new Properties();

    public PropertiesUtil() throws IOException {
        reader = new BufferedReader(new FileReader(ProjectSettings.PROPERTIES_PATH + getFileName()));
        properties.load(reader);
    }

    public String getFileName() {
        if(Objects.nonNull(RequestManager.shared().getProfile())){
            if (RequestManager.shared().getProfile().equals("LOCAL")) return LOCAL_PROPERTY_FILE_NAME;
            else return DEFAULT_PROPERTY_FILE_NAME;
        }else
            return LOCAL_PROPERTY_FILE_NAME;
    }

    public String getPropertyByName(String property) {
        String propertyName = properties.getProperty(property);

        if (propertyName != null) return propertyName;
        else throw new RuntimeException("Property not found: " + property);
    }


}
