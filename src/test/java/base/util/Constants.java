package base.util;

public class Constants {
    public static final String USERNAME_FIELD = "username";
    public static final String PASSWORD_FIELD = "password";
    public static final String MESSAGE = "message";
    public static final String SLASH = "/";
    public static final String PRODUCTS = "products";

}
