package test.product;

import base.ProjectSettings;
import com.github.javafaker.Faker;
import io.qameta.allure.Feature;
import io.qameta.allure.junit4.DisplayName;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import service.ProductService;
import suites.AllTests;
import suites.BugTests;
import suites.ContractTests;

import static base.util.Constants.MESSAGE;
import static base.util.Constants.PRODUCTS;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.*;

@Feature("Manage product flow")
public class ProductTest {
    ProductService productService;

    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String PRICE = "price";
    public static final String DISCOUNT_PERCENTAGE = "discountPercentage";
    public static final String RATING = "rating";
    public static final String STOCK = "stock";
    public static final String BRAND = "brand";
    public static final String CATEGORY = "category";

    @SneakyThrows
    @Before
    public void setUp() {
        productService = new ProductService();
    }

    @Test
    @Category(AllTests.class)
    @DisplayName("Retrieve all products successfully")
    public void retrieveAllProductsSuccessfully() {
        Response response = productService.getProductList();

        response
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body(PRODUCTS, notNullValue());
    }

    @Test
    @Category(AllTests.class)
    @DisplayName("Retrieve a product by id successfully")
    public void retrieveProductByIdSuccessfully() {
        Response response = productService.getProduct(5);

        response
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("id", is(5))
                .body("title", is("Huawei P30"));
    }

    @Test
    @Category(AllTests.class)
    @DisplayName("Retrieve non-existent product")
    public void searchForNonExistentProduct() {
        Response response = productService.getProduct(2000);

        response
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body(MESSAGE, is("Product with id '2000' not found"));
    }

    @Test
    @Category(AllTests.class)
    @DisplayName("Register product successfully")
    public void registerProductSuccessfully() {
        Faker faker = new Faker();

        String title = faker.commerce().productName();
        String description = faker.lorem().sentence();
        Integer price = faker.number().numberBetween(100, 10000);
        Float discountPercentage = faker.number().randomNumber(2, true) / 100.0f;
        Float rating = faker.number().randomNumber(2, true) / 100.0f;
        Integer stock = faker.number().numberBetween(0, 1000);
        String brand = faker.company().name();
        String category = faker.commerce().department();

        Response response = productService.postProduct(createPayload(title,
                description,
                price,
                discountPercentage,
                rating,
                stock,
                brand,
                category));

        response
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body(TITLE, is(title))
                .body(DESCRIPTION, is(description))
                .body(PRICE, equalTo(price))
                .body(DISCOUNT_PERCENTAGE, is(discountPercentage))
                .body(RATING, is(rating))
                .body(STOCK, is(stock))
                .body(BRAND, is(brand))
                .body(CATEGORY, is(category));

    }

    @Test
    @Category(BugTests.class)
    @DisplayName("Register a product with invalid data")
    public void registerProductWithInvalidData() {
        Response response = productService.postProduct(new JSONObject());

        response
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body(MESSAGE, is("Invalid payload"));
    }

    @Test
    @Category({AllTests.class, ContractTests.class})
    @DisplayName("Validate product query contract")
    public void validateProductQueryContract() {
        Response response = productService.getProductList();

        response
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body(matchesJsonSchemaInClasspath
                        (ProjectSettings.API_CONTRACT_PATH + "productsListContract.json"
                        ));
    }

    @Test
    @Category({AllTests.class, ContractTests.class})
    @DisplayName("Validate contract of product query")
    public void validateContractOfProductQuery() {
        Response response = productService.getProduct(10);

        response
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body(matchesJsonSchemaInClasspath
                        (ProjectSettings.API_CONTRACT_PATH + "productContract.json"
                        ));
    }

    private JSONObject createPayload(String title, String description, Integer price,
                                     Float discountPercentage, Float rating,
                                     Integer stock, String brand, String category) {
        JSONObject payload = new JSONObject();
        payload.put(TITLE, title);

        payload.put(DESCRIPTION, description);

        payload.put(PRICE, price);

        payload.put(DISCOUNT_PERCENTAGE, discountPercentage);

        payload.put(RATING, rating);

        payload.put(STOCK, stock);

        payload.put(BRAND, brand);

        payload.put(CATEGORY, category);

        return payload;
    }


}
