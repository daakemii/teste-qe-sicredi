package test;

import base.RequestManager;
import io.restassured.RestAssured;
import io.restassured.config.HttpClientConfig;
import org.junit.After;
import org.junit.Before;

import java.util.Optional;
import java.util.function.Predicate;

public class hooks {
    private static final String PROFILE_ENV = "profile";
    private static final String DEFAULT_PROFILE = "local";

    @Before
    public void setUp() {
        final var profile = Optional.ofNullable(System.getenv().get(PROFILE_ENV))
                .filter(Predicate.not(String::isEmpty)).orElse(DEFAULT_PROFILE);


        RequestManager.shared().setProfile(profile);

        RestAssured.config = RestAssured.config().httpClient(HttpClientConfig.httpClientConfig()
                .setParam("http.connection.timeout", 60000)
                .setParam("http.socket.timeout", 240000));
    }

    @After
    public void teardown() {
        RequestManager.shared().teardownRequest();
        RequestManager.shared().teardownRequestResponse();
    }

}
