package test.user;

import base.ProjectSettings;
import io.qameta.allure.Feature;
import io.qameta.allure.junit4.DisplayName;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import service.UserService;
import suites.AllTests;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;

@Feature("Manage user flow")
public class UserTest {

    UserService userService;
    private static Response response;

    @SneakyThrows
    @Before
    public void setUp() {
        userService = new UserService();
        response = userService.getUserList();
    }

    @Test
    @DisplayName("Retrieve user list")
    @Category(AllTests.class)
    public void retrieveUserList() {
        response
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body("users", notNullValue())
                .rootPath("users[0]")
                .body("firstName", is("Terry"))
                .body("lastName", is("Medhurst"))
                .body("maidenName", is("Smitham"))
                .body("age", is(50))
                .body("gender", is("male"));

    }

    @Test
    @DisplayName("Validate user query contract")
    @Category(AllTests.class)
    public void validateUserQueryContract() {
        response
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body(matchesJsonSchemaInClasspath
                        (ProjectSettings.API_CONTRACT_PATH + "usersListContract.json"
                        ));
    }

}
