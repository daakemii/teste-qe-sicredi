package test.applicationStatus;

import io.qameta.allure.Feature;
import io.qameta.allure.junit4.DisplayName;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import service.ApplicationStatusService;
import suites.AllTests;
import test.hooks;

import static org.hamcrest.CoreMatchers.is;

@Feature("Check application status")
public class ApplicationStatusTest extends hooks {

    ApplicationStatusService applicationStatusService;

    @SneakyThrows
    @Before
    public void setUp(){
        applicationStatusService = new ApplicationStatusService();
    }

    @Test
    @Category(AllTests.class)
    @DisplayName("Retrieve application status")
    public void retrieveApplicationStatus(){
       Response response = applicationStatusService.getTestStatus();
       response
               .then()
               .assertThat()
               .statusCode(HttpStatus.SC_OK)
               .body("status", is("ok"))
               .body("method", is("GET"));
    }
}
