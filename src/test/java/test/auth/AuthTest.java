package test.auth;

import base.util.PropertiesUtil;
import io.qameta.allure.Feature;
import io.qameta.allure.junit4.DisplayName;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import service.AuthService;
import service.UserService;
import suites.AllTests;

import java.util.HashMap;
import java.util.List;

import static base.util.Constants.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;

@Feature("Manage authentication flow")
public class AuthTest {

    AuthService authService;
    UserService userService;
    PropertiesUtil propertiesUtil;
    String username;
    String password;

    private final String TOKEN = "token";


    @SneakyThrows
    @Before
    public void setUp() {
        authService = new AuthService();
        userService = new UserService();
        propertiesUtil = new PropertiesUtil();
    }

    @Test
    @Category(AllTests.class)
    @DisplayName("Create authentication token successfully")
    public void createTokenForAuthenticationSuccess() {
        getUserCredentials();

        Response response = authService.doAuthentication(username, password);

        assertAuthenticationSuccess(response);
    }



    @Test
    @Category(AllTests.class)
    @DisplayName("Create authentication token using login from environment variables successfully")
    public void createTokenForAuthenticationUsingEnvironmentLoginSuccess() {
        setUsernameAndPasswordFromEnvironment();

        Response response = authService.doAuthentication(username, password);

        assertAuthenticationSuccess(response);
    }


    @Test
    @Category(AllTests.class)
    @DisplayName("Create token for invalid authentication")
    public void createTokenForInvalidAuthentication() {
        
        Response response = authService.doAuthentication("InvalidUser", "InvalidPassword");

        response.then()
                .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body(MESSAGE, is("Invalid credentials"));
    }

    @Test
    @Category(AllTests.class)
    @DisplayName("Retrieve products with authentication")
    public void retrieveProductsWithAuthentication() {
        setUsernameAndPasswordFromEnvironment();

        String token = authService.doAuthentication(username, password).jsonPath().get(TOKEN);

        Response response = authService.getUserAuthenticationProducts(token);

        response
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body(PRODUCTS, notNullValue());
    }

    @Test
    @Category(AllTests.class)
    @DisplayName("Retrieve products with invalid authentication")
    public void retrieveProductsWithInvalidAuthentication() {
        Response response = authService.getUserAuthenticationProducts("");

        response
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_UNAUTHORIZED)
                .body(MESSAGE, is("Invalid/Expired Token!"));
    }

    private void getUserCredentials() {
        List<HashMap<String, String>> hashMaps = userService.getUserList().jsonPath().getList("users");

        username = hashMaps.get(0).get(USERNAME_FIELD);
        password = hashMaps.get(0).get(PASSWORD_FIELD);
    }

    private void setUsernameAndPasswordFromEnvironment() {
        username = propertiesUtil.getPropertyByName("Sheldon.username");
        password = propertiesUtil.getPropertyByName("Sheldon.password");
    }

    private void assertAuthenticationSuccess(Response response) {
        response.then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body(TOKEN, notNullValue());
    }

}
